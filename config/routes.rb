Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users, only: [:create, :update, :show ,:findUser] do
    collection do
      post 'confirm'
      post 'login'
      get 'show'
      get 'showProfile'
      get 'findUser'
      get 'findGarage'
      get 'findCrane'
    end
  end

  resources :cars, only: [ :create, :update, :destroy]
  get 'cars/findPlate', to: 'cars#findPlate'

  resources :cranes, only: [:show, :create, :update, :destroy]
  resources :garages, only: [:show, :create, :update, :destroy,:findGarage]
  resources :transfers, only: [:show, :create]
  resources :cites, only: [:show, :create, :update, :destroy]
  get 'cite/showCitesByUser', to: 'cites#showCitesByUser'

  resources :records, only: [:show, :create, :update, :destroy]
  get 'record/showRecordByUser', to: 'records#showRecordByUser'
  get 'record/showRecordByCar', to: 'records#showRecordByCar'

  post 'password/forgot', to: 'passwords#forgot'
  post 'password/forgot', to: 'passwords#forgot'

  put 'password/update', to: 'passwords#update'

end
