# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170327015117) do

  create_table "cars", force: :cascade do |t|
    t.string   "plate",      null: false
    t.string   "mark"
    t.string   "year"
    t.string   "model"
    t.string   "color"
    t.string   "cylinder"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id"
    t.index ["id"], name: "index_cars_on_id"
    t.index ["user_id"], name: "index_cars_on_user_id"
  end

  create_table "cites", force: :cascade do |t|
    t.integer  "garage_id"
    t.integer  "user_id"
    t.date     "date_now"
    t.string   "reservation_date"
    t.string   "description"
    t.string   "hour"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["garage_id"], name: "index_cites_on_garage_id"
    t.index ["user_id"], name: "index_cites_on_user_id"
  end

  create_table "cranes", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "direction"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "phone"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_cranes_on_user_id"
  end

  create_table "garages", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.string   "direction"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "phone"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_garages_on_user_id"
  end

  create_table "records", force: :cascade do |t|
    t.integer  "car_id"
    t.integer  "garage_id"
    t.datetime "date"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["car_id"], name: "index_records_on_car_id"
    t.index ["garage_id"], name: "index_records_on_garage_id"
  end

  create_table "transfers", force: :cascade do |t|
    t.datetime "date"
    t.integer  "ca_id"
    t.integer  "user_effector_id"
    t.integer  "user_transferred_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["user_effector_id"], name: "index_transfers_on_user_effector_id"
    t.index ["user_transferred_id"], name: "index_transfers_on_user_transferred_id"
    t.index [nil], name: "index_transfers_on_car_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email",                                  null: false
    t.string   "password_digest",                        null: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.boolean  "admin",                  default: false
    t.boolean  "user",                   default: false
    t.boolean  "crane_user",             default: false
    t.boolean  "garage_user",            default: false
  end

end
