class CreateCites < ActiveRecord::Migration[5.0]
  def change
    create_table :cites do |t|
      t.integer :garage_id
      t.integer   :user_id
      t.date :date_now
      t.string :reservation_date
      t.string  :description
      t.string  :hour
      t.timestamps
    end
  end
end
