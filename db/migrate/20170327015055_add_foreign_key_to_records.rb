class AddForeignKeyToRecords < ActiveRecord::Migration[5.0]
  def change
    add_index :records, :garage_id
    add_index :records, :car_id
  end
end
