class AddForeignKeyToGarages < ActiveRecord::Migration[5.0]
  def change
    add_index :garages, :user_id
  end
end
