class CreateTransfers < ActiveRecord::Migration[5.0]
  def change
    create_table :transfers do |t|
      t.datetime :date
      t.integer :ca_id
      t.integer :user_effector_id
      t.integer :user_transferred_id
      t.timestamps
    end
  end
end
