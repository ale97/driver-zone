class CreateCars < ActiveRecord::Migration[5.0]
  def change
    create_table :cars do |t|
  t.string :id_user,    null: false
  t.string :plate , null:false
  t.string :mark
  t.string :year
  t.string :model
  t.string :color
  t.string :cylinder

      t.timestamps
    end
  end
end
