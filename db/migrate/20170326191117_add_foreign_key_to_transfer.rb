class AddForeignKeyToTransfer < ActiveRecord::Migration[5.0]
  def change
    add_index :transfers, :user_transferred_id
    add_index :transfers, :user_effector_id
    add_index :transfers, :car_id
  end
end
