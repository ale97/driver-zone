class AddGarageToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :garage_user, :boolean, default: false
  end
end
