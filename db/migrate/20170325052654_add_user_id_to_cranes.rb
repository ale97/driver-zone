class AddUserIdToCranes < ActiveRecord::Migration[5.0]
  def change
    add_column :cranes, :user_id, :integer
  end
end
