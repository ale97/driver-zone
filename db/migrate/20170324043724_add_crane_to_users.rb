class AddCraneToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :crane_user, :boolean, default: false
  end
end
