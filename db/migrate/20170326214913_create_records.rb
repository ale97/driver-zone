class CreateRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :records do |t|
      t.integer :car_id
      t.integer   :garage_id
      t.datetime :date
      t.string  :description
      t.timestamps
    end
  end
end
