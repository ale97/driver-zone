class AddUserIdToGarage < ActiveRecord::Migration[5.0]
  def change
    add_column :garages, :user_id, :integer
  end
end
