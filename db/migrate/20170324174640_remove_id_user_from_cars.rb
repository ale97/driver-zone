class RemoveIdUserFromCars < ActiveRecord::Migration[5.0]
  def change
    remove_column :cars, :id_user, :string
  end
end
