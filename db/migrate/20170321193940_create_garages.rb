class CreateGarages < ActiveRecord::Migration[5.0]
  def change
    create_table :garages do |t|
      t.string :name
      t.string :email
      t.string :direction
      t.string :latitude
      t.string :longitude
      t.string :phone
      t.string :description  
      t.timestamps
    end
  end
end
