class AddForeignKeyToCites < ActiveRecord::Migration[5.0]
  def change
    add_index :cites, :garage_id
    add_index :cites, :user_id
  end
end
