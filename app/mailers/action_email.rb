class ActionEmail < ActionMailer::Base
  #default from: "presaguirre@gmail.com"

  def welcome_email(user)
    @user = user
    #@url = 'http://codeHero.co'

    attachments["logoApp.png"] = File.read("#{Rails.root}/public/logoApp.png")

    email_with_name = "#{@user.name} <#{@user.email}>"

    mail(to: email_with_name, from: ""+@user.email,
         subject: 'Confirmación de cuenta, DriverZone.! ')


  end

  def forgot_password(user)
    @user = user


    #attachments["logoApp.png"] = File.read("#{Rails.root}/public/logoApp.png")

    email_with_name = "#{@user.name} <#{@user.email}>"

    mail(to: email_with_name, from: ""+@user.email,
         subject: 'Restablecimiento de Contraseña')

  end

end
