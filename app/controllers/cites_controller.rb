class CitesController < ApplicationController
  before_action :authenticate_request!
  before_action :is_user, only: [:showCitesByUser,:create]
  before_action :is_garage, only: [:show]
  before_action :is_admin, only: [:destroy]

  #Load all cites to garage
  def show
    @cites = Cite.find_by_sql ('SELECT C.id,G.name,G.phone,G.direction,U.name,U.email,C.description,C.date_now,C.hour
    FROM  garages G, users U,cites C
    WHERE C.user_id = U.id AND G.id = C.garage_id AND G.user_id= ' + current_user.id.to_s)
    render json: {status: 'SUCCESS', message: 'Loaded all cites', data: @cites}, status: :ok
  end

  #Load all cites this user
  def showCitesByUser
    @cites = Record.find_by_sql ('SELECT G.id,G.name,G.phone,G.direction,U.name,U.email,C.description,C.date_now,C.hour
    FROM  garages G, users U,cites C
    WHERE C.user_id = U.id AND G.id = C.garage_id AND C.user_id= ' + current_user.id.to_s)
    render json: {status: 'SUCCESS', message: 'Loaded all cites', data: @cites}, status: :ok
  end
  #Only user create cite
  def create
    cite = Cite.new(cite_params)
    cite.user_id =current_user.id
    cite.date_now =Time.now
    if cite.save
      #Invoke send email method here
      render json: {status: 'cite created successfully'}, status: :created
    else
      render json: {errors: cite.errors.full_messages}, status: :bad_request
    end
  end

  def update
    @cites = Cite.find(params[:id])
    if @cites.update_attributes(cite_params)
      render json: {status: 'successful update'}, status: :ok
    else
      render json: {errors: @cites.errors.full_messages}, status: :bad_request
    end
  end

  def destroy
    Cite.destroy
    render json: {status: 'Cite deleted'}, status: :ok
  end

  private
  def cite_params
    params.require(:cite).permit(:garage_id, :reservation_date, :description, :hour)
  end

  def is_user
    unless current_user.user?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end

  def is_garage
    unless current_user.garage?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end

  def is_admin

    unless current_user.admin?
      render json: {error: 'No tiene acceso a esta pagina'}
    end

  end


end
