class TransfersController < ApplicationController
  before_action :authenticate_request!
  before_action :is_user, only: [:create,:show]

  def show
    @transfer = Transfer.find_by_sql ('SELECT C.model,C.plaque,U.name,U.email
    FROM  cars C, users U
    WHERE C.user_id = '+ current_user.id.to_s + '  AND U.id = C.user_id')
    render json: {status: 'SUCCESS', message: 'Loaded all transfer', data: @transfer}, status: :ok
  end


  def create
    transfer = Transfer.new(transfer_params)
    transfer.date
    transfer.user_effector_id=current_user.id
    transfer.carTransfer(transfer.user_transferred_id,transfer.car_id)
    if transfer.save
      #Invoke send email method here
      render json: {status: 'transfer created successfully'}, status: :created
    else
      render json: {errors: transfer.errors.full_messages}, status: :bad_request
    end
  end



  private
  def transfer_params
    params.require(:transfer).permit(:car_id, :user_transferred_id)
  end
  def is_user
    unless current_user.user?
      render json: {error: 'No tiene acceso a esta pagina' }
    end
  end

end
