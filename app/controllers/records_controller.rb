class RecordsController < ApplicationController
  before_action :authenticate_request!
  #before_action :is_garage, only: [:create,:update,:show]
  before_action :is_admin, only: [:destroy]


  #load all records created by garage
  def show
    @records = Record.find_by_sql ('SELECT G.name,G.phone,G.direction,R.description,C.model,C.id,C.plate
    FROM  garages G, records R,cars C
    WHERE G.id = R.garage_id AND   C.id = R.car_id AND G.user_id= '+ current_user.id.to_s)
    render json: {status: 'SUCCESS', message: 'Loaded all Records', data: @records}, status: :ok
  end

  #Load all records this user
  def showRecordByUser
    @records = Record.find_by_sql ('SELECT G.name,G.phone,G.direction,R.description,C.model,C.id,C.plate
    FROM  garages G, records R,cars C
    WHERE G.id = R.garage_id AND   C.id = R.car_id AND C.user_id= ' + current_user.id.to_s)
    render json: {status: 'SUCCESS', message: 'Loaded all Records', data: @records}, status: :ok
  end

  #Load records by select car
  def showRecordByCar
    car_id=params[:car_id]
    @records = User.find_by_sql ("SELECT G.name,G.phone,G.direction,R.description,C.model,C.id,C.plate
    FROM  garages G, records R,cars C
    WHERE G.id = R.garage_id AND R.car_id=C.id AND  R.car_id = '#{car_id}' ")
    if @records.blank?
      render json: {status: 'SUCCESS', message: 'No record was found'}, status: :bad_request
    else
      render json: {status: 'SUCCESS', message: 'Load  Record', data: @records}, status: :ok
    end
  end


  def create
    record = Record.new(record_params)
    if record.save
      #Invoke send email method here
      render json: {status: 'Record created successfully'}, status: :created
    else
      render json: {errors: record.errors.full_messages}, status: :bad_request
    end
  end

  def update
    @records = Record.find(params[:id])
    if @records.update_attributes(record_params)
      render json: {status: 'successful update'}, status: :ok
    else
      render json: {errors: @records.errors.full_messages}, status: :bad_request
    end
  end

  def destroy
    Record.destroy
    render json: {status: 'Record deleted'}, status: :ok
  end

  private
  def record_params
    params.require(:record).permit(:garage_id, :date, :description, :car_id)
  end

  def is_garage
    unless current_user.garage?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end

  def is_admin
    unless current_user.admin?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end

  def is_user
    unless current_user.user?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end

end
