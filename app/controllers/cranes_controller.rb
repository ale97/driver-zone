class CranesController < ApplicationController
  before_action :authenticate_request!
  before_action :is_crane, only: [:create, :update, :show]
  before_action :is_admin, only: [:destroy]


  def show
    @crane = Crane.find_by_sql ('SELECT C.name,C.id, direction, phone, U.name,U.email
    FROM users U, cranes C
    WHERE U.id = C.user_id AND U.id='+current_user.id.to_s)


    render json: {status: 'SUCCESS', message: 'Loaded all Cars', data: @crane}, status: :ok
  end

  def create
    crane = Crane.new(crane_params)
    crane.user_id= current_user.id
    if crane.save
      # ActionEmail.welcome_email(crane).deliver
      render json: {status: 'Crane created successfully'}, status: :created
    else
      render json: {errors: crane.errors.full_messages}, status: :bad_request
    end
  end

# PATCH/PUT /cranes/1.json
  def update
    @crane = Crane.find(params[:id])
    @crane.user_id=current_user.id
    if @crane.update_attributes(crane_params)
      render json: {status: 'successful update'}, status: :ok
    else
      render json: {errors: @crane.errors.full_messages}, status: :bad_request
    end
  end

# DELETE /cranes/1.json
  def destroy
    @crane.destroy
    render json: {status: 'Crane deleted'}, status: :ok
  end


  private

  def crane_params
    params.require(:crane).permit(:direction, :latitude, :longitude, :description, :phone, :name)
  end

  def is_crane
    unless current_user.crane_user?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end

  def is_admin
    unless current_user.admin?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end


end
