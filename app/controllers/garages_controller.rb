class GaragesController < ApplicationController

  before_action :authenticate_request!
  before_action :is_garage, only: [:create, :update,:show]
  before_action :is_admin, only: [:destroy]

  #Load your garages
  def show
    @garage = Garage.find_by_sql ('SELECT G.name,G.id, direction, phone, U.name,U.email
    FROM users U, garages G
    WHERE U.id = G.user_id AND U.id='+current_user.id.to_s)

    render json: {status: 'SUCCESS', message: 'Loaded all Cars', data: @garage}, status: :ok
  end


  def create
    garage = Garage.new(garage_params)
    garage.user_id= current_user.id
    if garage.save
      render json: {status: 'garage created successfully'}, status: :created
    else
      render json: {errors: garage.errors.full_messages}, status: :bad_request
    end
  end

# PATCH/PUT /garages/1.json
  def update
    @garage = Garage.find(params[:id])
    if @garage.update_attributes(garage_params)
      render json: {status: 'update successfully'}, status: :ok
    else
      render json: {errors: @garage.errors.full_messages}, status: :bad_request
    end
  end

# DELETE /users/1.json
  def destroy
    @garage.destroy
    render json: {status: 'Garage deleted'}, status: :ok
  end

  private

  def garage_params
    params.require(:garage).permit(:direction, :latitude, :longitude, :description, :phone, :name)
  end

  def is_garage
    unless current_user.garage?
      render json: {error: 'No tiene acceso a esta pagina' }
    end
  end

  def is_admin
    unless current_user.admin?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end

end
