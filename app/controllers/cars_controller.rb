class CarsController < ApplicationController
  before_action :authenticate_request!
  before_action :is_user, only: [:show, :create]
  before_action :is_garage, only: [:findPlate]
  before_action :is_admin, only: [:update]

  def show

    @cars = Car.find_by_sql ('SELECT c.id,plate, model, mark, U.name,U.email
    FROM users U, cars C
    WHERE U.id = C.user_id AND U.id='+current_user.id.to_s)


    render json: {status: 'SUCCESS', message: 'Loaded all Cars', data: @cars}, status: :ok
  end

  def findPlate
    plate=params[:plate]
    @cars = Car.find_by_sql ("SELECT id,plate, model, mark,year
    FROM  cars
    WHERE cars.plate= '#{plate}'")

      render json: {status: 'SUCCESS', message: 'Load  Cars', data: @cars}, status: :ok
  end


  def create
    car = Car.new(car_params)
    car.user_id=current_user.id

    if car.save
      render json: {status: 'Car created successfully'}, status: :created
    else

      render json: {errors: car.errors.full_messages}, status: :bad_request
    end
  end

  # PATCH/PUT /cars/1.json
  def update
    @cars= Car.find(params[:id])
    if @cars.update_attributes(car_params)
      render json: {status: 'successful update'}, status: :ok
    else
      render json: {errors: @cars.errors.full_messages}, status: :bad_request
    end
  end


  private
  def car_params
    params.require(:car).permit(:plate, :mark, :model, :color, :cylinder,:year)
  end

  def is_user
    unless current_user.user?
      render json: {error: 'No tiene acceso a esta pagina '}
    end

  end

  def is_garage
    unless current_user.garage?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end


  def is_admin
    unless current_user.admin?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end


end
