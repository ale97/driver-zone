class UsersController < ApplicationController

  before_action :authenticate_request!, except: [:create, :confirm, :login]
  before_action :authenticate_request!, only: [:show, :showProfile,:findUser,:findGarage]

  before_action :is_admin, only: [:show]

  # GET /users.json
  #Only admin user show all user
  def show
    @users = User.all
    render json: {status: 'SUCCESS', message: 'Loaded all users', data: @users}, status: :ok
  end



  def showProfile
    @users = User.find_by_sql ('SELECT U.name,U.email,U.id
    FROM users U
    WHERE U.id='+current_user.id.to_s)
    render json: {status: 'SUCCESS', message: 'Loaded my Profile', data: @users}, status: :ok
  end

  #para que el taller busque un usuario y le asigne una cita
  def findUser
    email=params[:email]
    @users = User.find_by_sql ("SELECT id,name, email
    FROM  users
    WHERE  users.user = 't' AND users.email = '#{email}' ")
    if @users.blank?
      render json: {status: 'SUCCESS', message: 'No User was found'}, status: :ok

    else
      render json: {status: 'SUCCESS', message: 'Load  User', data: @users}, status: :ok
    end
  end
  #find crane by email

  def findCrane
    email=params[:email]
    @users = User.find_by_sql (" SELECT U.id,U.name, U.email,C.name,C.phone
  FROM  users U, cranes C
  WHERE  U.crane_user = 't' AND U.id = C.user_id
  AND U.email = #{email}' ")
    if @users.blank?
      render json: {status: 'SUCCESS', message: 'No User was found'}, status: :ok

    else
      render json: {status: 'SUCCESS', message: 'Load  User', data: @users}, status: :ok
    end
  end

  #find garage by email
  def findGarage
    email=params[:email]
    users = User.find_by_sql ("SELECT U.id,U.name, U.email,G.name,G.phone
    FROM  users U, garages G
    WHERE  U.garage_user = 't' AND U.id = G.user_id
       AND U.email = '#{email}' ")
    if users.blank?
      render json: {status: 'SUCCESS', message: 'No garage was found'}, status: :ok
    else
      render json: {status: 'SUCCESS', message: 'Load  Garage', data: users}, status: :ok
    end
  end


  def create
    user = User.new(user_params)

    if user.save
      #Invoke send email method here
      ActionEmail.welcome_email(user).deliver
      render json: {status: 'User created successfully, Ckeck your Email'}, status: :created
    else

      render json: {errors: user.errors.full_messages}, status: :bad_request
    end
  end


  def confirm
    token = params[:token].to_s

    user = User.find_by(confirmation_token: token)

    if user.present? && user.confirmation_token_valid?
      user.mark_as_confirmed!
      render json: {status: 'User confirmed successfully'}, status: :ok
    else
      render json: {status: 'Invalid token'}, status: :not_found
    end
  end

  def login
    user = User.find_by(email: params[:email].to_s.downcase)

    if user && user.authenticate(params[:password])
      if user.confirmed_at?
        auth_token = JsonWebToken.encode({user_id: user.id})

        @users=user
        if @users.user?
          render json: {auth_token: auth_token,name:@users.name,email:@users.email,id:@users.id,user:1 }, status: :ok
        elsif @users.garage_user?
          render json: {auth_token: auth_token,name:@users.name,email:@users.email,id:@users.id,user:2}, status: :ok
        elsif @users.crane_user?
          render json: {auth_token: auth_token,name:@users.name,email:@users.email,id:@users.id,user:3 }, status: :ok
        else
          render json: {auth_token: auth_token,name:@users.name,email:@users.email,id:@users.id,user:0}, status: :ok
        end

      else
        render json: {error: 'Email not verified'}, status: :unauthorized
      end
    else
      render json: {error: 'Invalid username / password'}, status: :unauthorized
    end
  end


  private
  def user_params
    params.require(:user).permit(:email, :name, :password, :password_confirmation, :admin, :crane_user, :garage_user, :user)
  end

  def is_user
    unless current_user.user?
      render json: {error: 'No tiene acceso a esta pagina'}
    end
  end

  def is_admin

    unless current_user.admin?
      render json: {error: 'No tiene acceso a esta pagina'}
    end

  end

end
