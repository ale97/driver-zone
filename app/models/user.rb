class User < ApplicationRecord
  has_many :cars
  has_one :crane
  has_one :garage
  has_many :transfer
  has_many :cites


  has_secure_password
  before_save :downcase_email
  before_create :generate_confirmation_instructions

  def downcase_email
    self.email = self.email.delete(' ').downcase
  end

  def generate_confirmation_instructions
    self.confirmation_token = SecureRandom.hex(10)
    self.confirmation_sent_at = Time.now.utc
  end

  def confirmation_token_valid?
    (self.confirmation_sent_at + 30.days) > Time.now.utc
  end

  def mark_as_confirmed!
    self.confirmation_token = nil
    self.confirmed_at = Time.now.utc
    save!
  end


  validates_presence_of :email
  validates_presence_of :name
  validates_uniqueness_of :email, case_sensitive: false
  validates_format_of :email, with: /@/


  def generate_password_token!
    self.reset_password_token = generate_token
    self.reset_password_sent_at = Time.now.utc
    save!
  end

  def password_token_valid?
    (self.reset_password_sent_at + 4.hours) > Time.now.utc
  end

  def reset_password!(password)
    self.reset_password_token = nil
    self.password = password
    save!
  end

  def admin?
    admin
  end

  def user?
    user
  end

  def crane_user?
    crane_user
  end

  def garage?
    garage_user
  end

  private

  def generate_token
    SecureRandom.hex(10)
  end

end
