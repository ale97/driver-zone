class Cite < ApplicationRecord
  belongs_to :user
  has_many :garages

  validates_presence_of :garage_id
  validates_presence_of :user_id
  validates_presence_of :reservation_date
  validates_presence_of :description
  validates_presence_of :hour

end
