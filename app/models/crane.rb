class Crane < ApplicationRecord
  belongs_to :user


  validates_presence_of :name
  validates_presence_of :user_id
  validates_presence_of :direction
  validates_presence_of :phone
  validates_presence_of :description
  validates_presence_of :latitude
  validates_presence_of :longitude

end
