class Car < ApplicationRecord
  belongs_to :user
  has_many :transfer
  has_many :records

  validates_presence_of :plate
  validates_presence_of :year
  validates_presence_of :mark
  validates_presence_of :color
  validates_presence_of :user_id
  validates_presence_of :model


end
