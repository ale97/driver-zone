class Record < ApplicationRecord
  has_one :car
  has_one :garage

  validates_presence_of :car_id
  validates_presence_of :garage_id
  validates_presence_of :date
  validates_presence_of :description

end